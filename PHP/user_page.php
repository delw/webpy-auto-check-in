<?php
// 引入加载器
require_once dirname(__FILE__) . "/core/loader.php";

// 引导用户
if (!check_login()){header("Location: login_page.php");}

// 修改密码
$warning = "";

// post请求 登录修改密码
if ($_SERVER['REQUEST_METHOD'] == 'POST' and $_POST['action'] == 'repassword'){
	if (trim($_POST['password']) == ''){
		
	}else{
		sql_query("UPDATE `webpy_user` SET `password`='"  . md5($_POST['password']) . "' WHERE `username` = '" . $user_name . "'");
		$warning = '密码已修改！下次请使用新密码登录！';
	}
}
?>

<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Python自动化程序管理后台主页 - 以赏的秘密小屋</title>

    <meta name="description" content="Python自动化程序">
    <meta name="author" content="我叫以赏">
	
	<link rel="shortcut icon" href="<?php echo $web_icon;?>" type="image/x-icon">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	<style>body{margin: 20px 20px 20px 20px;min-width: 1600px;}div{width : 100%;max-width: 1600px;}</style>
  </head>
  
  <body>

    <div class="container-fluid">
	<div class="row-fluid">
		<div class="col-md-12">
			<div class="alert alert-dismissable alert-info">		
			<h4>公告</h4><?php echo $dashboard_notice ; //引入公告 ?></div>
			<ul class="nav nav-pills" style="margin-bottom: 15px;">
				<li class="nav-item"><a class="nav-link" href="./index.php">数据总览页</a></li>
				<li class="nav-item"><a class="nav-link" href="./log_page.php">日志查看页</a></li>
				<li class="nav-item"><a class="nav-link" href="./setting_page.php">程序设置页</a></li>
				<li class="nav-item"><a class="nav-link active" href="./user_page.php">用户信息页</a></li>
				<p class="nav-link"><?php echo "登录用户：" . $user_name ?></p>
				
				<li class="nav-item dropdown ml-md-auto">
					 <a class="nav-link dropdown-toggle"id="navbarDropdownMenuLink" data-toggle="dropdown">其它选项</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
						 
						 <a class="dropdown-item" href="http://blog.zhangyishang.top">项目首页</a> 
						 <a class="dropdown-item" href="#">赞助开发者</a> 
						 <div class="dropdown-divider"></div> 
						 <a class="dropdown-item" href="javascript:logout()">退出登录</a>
						 
					</div>
				</li>
				
			</ul>
			<?php if ($user_notice != ''){ ?>
			<div class="alert alert-dismissable alert-warning">
				<h4>系统提醒</h4> 
				<?php echo $user_notice; ?>
				</div> 
			</div>
			<?php } ?>
			<div class="container-fluid" style="margin: 10px 10px 10px 10px;">
				
				<div class="row">
					<div class="col-md-12">
						<h3>用户基本信息</h3>
						<p style="margin-top: 15px;">登录账号：<?php echo $user_name; ?></p>
						<p>注册时间：<?php echo $user_regtime; ?></p>
						<p><span class="badge badge-<?php 
							echo ['warning">网站管理员','default">未激活用户','info">正式会员','success">试用高级会员','primary">高级会员'][$user_level+1];
						?></span></p>
					
					
						<h3>用户信息修改</h3>
						<form method="post" role="form" style="margin-top: 15px;">
							<div class="form-group"> 
								<label for="password">修改密码</label>
								<input type="hidden" class="form-control" name="action" value="repassword">
								<input type="password" class="form-control" name="password" placeholder="Enter your password ......">
							</div>
							<p><span style="color:#E53333;"><strong><?php echo $warning; ?></strong></span></p>
							<button type="submit" class="btn btn-primary">提交修改</button>
						</form>
					</div>
				</div>
			</div>
		</div>


		
	
	</div>
	</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
	<script>function logout(){document.location.href = 'login_page.php?action=logout'}</script>
  </body>
</html>